# README #

### Secure python communication ###
This program allows two parties to communicate securely between them. It allows one party to be the "server" and set up the mode of communication (via chat or by once sending a png, jpg, txt, jpg file). All this communication is done securely, via a commitment scheme. The system is secure against man-in-the-middle attacks that might try to emulate the server. 

### How to run ###
Example on how to send a PNG file securely:

--> On server side:
    > python server.py -t png

--> On client side:
    > python client.py localhost 8888 -f myfile.png

To see the output, go to the folder where server.py is and notice the file 'output.png' which should be the same as 'myfile.png'